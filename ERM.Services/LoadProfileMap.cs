﻿using CsvHelper.Configuration;
using ERM.Entities.Models;

namespace ERM.Services
{
    public sealed class LoadProfileMap : ClassMap<LoadProfile>
    {
        public LoadProfileMap()
        {
            Map(x => x.MeterPointCode).Name("MeterPoint Code");
            Map(x => x.SerialNumber).Name("Serial Number");
            Map(x => x.PlantCode).Name("Plant Code");
            Map(x => x.DateTime).Name("Date/Time").TypeConverterOption.Format("dd/MM/yyyy HH:mm:ss");
            Map(x => x.DataType).Name("Data Type");
            Map(x => x.DataValue).Name("Data Value");
            Map(x => x.Units).Name("Units");
            Map(x => x.Status).Name("Status").Optional();
        }
    }
}