﻿using CsvHelper.Configuration;
using ERM.Entities.Models;

namespace ERM.Services
{
    public sealed class TouMap : ClassMap<Tou>
    {
        public TouMap()
        {
            Map(x => x.MeterCode).Name("MeterCode");
            Map(x => x.Serial).Name("Serial");
            Map(x => x.PlantCode).Name("PlantCode");
            Map(x => x.DateTime).Name("DateTime").TypeConverterOption.Format("dd/MM/yyyy H:mm");
            Map(x => x.Quality).Name("Quality");
            Map(x => x.Stream).Name("Stream");
            Map(x => x.DataType).Name("DataType");
            Map(x => x.Energy).Name("Energy");
            Map(x => x.Units).Name("Units");
        }
    }
}