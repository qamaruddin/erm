﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using ERM.Entities.Models;
using ERM.Entities.Repositories;

namespace ERM.Services
{
    public class DataImporter<T, TMap>
        where T : class
    where TMap : ClassMap<T>
    {
        private readonly IBulkImportRepository<T> _bulkImportRepository;

        public DataImporter(IBulkImportRepository<T> bulkImportRepository)
        {
            _bulkImportRepository = bulkImportRepository;
        }

        public async Task<DataImportResponse> Import(Stream memoryStream)
        {
            if (memoryStream == null)
            {
                throw new ArgumentException("Stream can't be null");
            }

            var response = new DataImportResponse();

            using (var streamReader = new StreamReader(memoryStream))
            {
                using (var csv = new CsvReader(streamReader))
                {
                    csv.Configuration.RegisterClassMap<TMap>();
                    var data = csv.GetRecords<T>().ToList();

                    var result = await _bulkImportRepository.Import(data);
                    response.Status = result > 0
                        ? ImportStatus.Success
                        : ImportStatus.Fail;
                }
            }

            return response;
        }
    }
}
