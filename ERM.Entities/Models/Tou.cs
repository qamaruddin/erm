﻿using System;

namespace ERM.Entities.Models
{
    public class Tou
    {
        public int TouId { get; set; }

        public int MeterCode { get; set; }

        public int Serial { get; set; }

        public string PlantCode { get; set; }

        public DateTime DateTime { get; set; }

        public string Quality { get; set; }

        public string Stream { get; set; }

        public string DataType { get; set; }

        public decimal Energy { get; set; }

        public string Units { get; set; }

    }
}
