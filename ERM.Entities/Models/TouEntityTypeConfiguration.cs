﻿using System.Data.Entity.ModelConfiguration;

namespace ERM.Entities.Models
{
    public class TouEntityTypeConfiguration : EntityTypeConfiguration<Tou>
    {
        public TouEntityTypeConfiguration()
        {
            HasKey(x => x.TouId);
            Property(x => x.Energy).HasPrecision(18, 6);
            Property(x => x.PlantCode).IsRequired();
            Property(x => x.Quality).IsRequired();
            Property(x => x.Stream).IsRequired();
            Property(x => x.DataType).IsRequired();
            Property(x => x.Units).IsRequired();
        }
    }
}