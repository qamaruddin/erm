﻿using System;

namespace ERM.Entities.Models
{
    public class ComputedAggregation
    {
        public int MeterCode { get; set; }

        public DateTime Date { get; set; }

        public string DataType { get; set; }

        public decimal Min { get; set; }

        public decimal Max { get; set; }

        public decimal Avg { get; set; }

    }
}
