﻿using System;

namespace ERM.Entities.Models
{
    public class LoadProfile
    {
        public int LoadProfileId { get; set; }

        public int MeterPointCode { get; set; }

        public int SerialNumber { get; set; }

        public string PlantCode { get; set; }

        public DateTime DateTime { get; set; }

        public string DataType { get; set; }

        public decimal DataValue { get; set; }

        public string Units { get; set; }

        public string Status { get; set; }

    }
}