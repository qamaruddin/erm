﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ERM.Entities.Models
{
    public class ComputedAggregationEntityTypeConfiguration : EntityTypeConfiguration<ComputedAggregation>
    {
        public ComputedAggregationEntityTypeConfiguration()
        {
            HasKey(x => new { x.MeterCode, x.Date, x.DataType });
            Property(x => x.MeterCode).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.MeterCode).IsRequired();
            Property(x => x.DataType).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.Min).HasPrecision(18, 6);
            Property(x => x.Max).HasPrecision(18, 6);
            Property(x => x.Avg).HasPrecision(18, 6);
        }
    }
}