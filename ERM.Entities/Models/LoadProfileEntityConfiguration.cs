﻿using System.Data.Entity.ModelConfiguration;

namespace ERM.Entities.Models
{
    public class LoadProfileEntityConfiguration : EntityTypeConfiguration<LoadProfile>
    {
        public LoadProfileEntityConfiguration()
        {
            this.HasKey(x => x.LoadProfileId);
            Property(x => x.DataValue).HasPrecision(18, 6);
            Property(x => x.Status).IsOptional();
            Property(x => x.DataType).IsRequired();
            Property(x => x.PlantCode).IsRequired();
            Property(x => x.Units).IsRequired();
        }
    }
}