﻿using System.Data.Entity;
using System.Reflection;
using ERM.Entities.Models;

namespace ERM.Entities
{
    public class ErmDbContext : DbContext
    {
        public ErmDbContext() : base("default")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }

        public virtual DbSet<LoadProfile> LoadProfiles { get; set; }

        public virtual DbSet<Tou> Tous { get; set; }

        public virtual DbSet<ComputedAggregation> ComputedAggregations { get; set; }
    }
}
