﻿namespace ERM.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialScript : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ComputedAggregations",
                c => new
                    {
                        MeterCode = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        DataType = c.String(nullable: false, maxLength: 128),
                        Min = c.Decimal(nullable: false, precision: 18, scale: 6),
                        Max = c.Decimal(nullable: false, precision: 18, scale: 6),
                        Avg = c.Decimal(nullable: false, precision: 18, scale: 6),
                    })
                .PrimaryKey(t => new { t.MeterCode, t.Date, t.DataType });
            
            CreateTable(
                "dbo.LoadProfiles",
                c => new
                    {
                        LoadProfileId = c.Int(nullable: false, identity: true),
                        MeterPointCode = c.Int(nullable: false),
                        SerialNumber = c.Int(nullable: false),
                        PlantCode = c.String(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        DataType = c.String(nullable: false),
                        DataValue = c.Decimal(nullable: false, precision: 18, scale: 6),
                        Units = c.String(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.LoadProfileId);
            
            CreateTable(
                "dbo.Tous",
                c => new
                    {
                        TouId = c.Int(nullable: false, identity: true),
                        MeterCode = c.Int(nullable: false),
                        Serial = c.Int(nullable: false),
                        PlantCode = c.String(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        Quality = c.String(nullable: false),
                        Stream = c.String(nullable: false),
                        DataType = c.String(nullable: false),
                        Energy = c.Decimal(nullable: false, precision: 18, scale: 6),
                        Units = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TouId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tous");
            DropTable("dbo.LoadProfiles");
            DropTable("dbo.ComputedAggregations");
        }
    }
}
