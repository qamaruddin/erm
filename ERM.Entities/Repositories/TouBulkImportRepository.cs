﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ERM.Entities.Models;

namespace ERM.Entities.Repositories
{
    public class TouBulkImportRepository : IBulkImportRepository<Tou>
    {
        private readonly ErmDbContext _dbContext;
        private readonly ComputedAggregationRepository _computedAggregationRepository;

        public TouBulkImportRepository(ErmDbContext dbContext, ComputedAggregationRepository computedAggregationRepository)
        {
            _dbContext = dbContext;
            _computedAggregationRepository = computedAggregationRepository;
        }

        public async Task<int> Import(IEnumerable<Tou> loadProfiles)
        {
            try
            {
                using (var sqlBulkCopy = new SqlBulkCopy(_dbContext.Database.Connection.ConnectionString, copyOptions: SqlBulkCopyOptions.UseInternalTransaction))
                {
                    var dt = new DataTable();
                    dt.Columns.Add("MeterCode", typeof(int));
                    dt.Columns.Add("Serial", typeof(int));
                    dt.Columns.Add("PlantCode", typeof(string));
                    dt.Columns.Add("DateTime", typeof(DateTime));
                    dt.Columns.Add("Quality", typeof(string));
                    dt.Columns.Add("Stream", typeof(string));
                    dt.Columns.Add("DataType", typeof(string));
                    dt.Columns.Add("Energy", typeof(decimal));
                    dt.Columns.Add("Units", typeof(string));

                    foreach (var loadProfile in loadProfiles)
                    {
                        var row = dt.NewRow();
                        row["MeterCode"] = loadProfile.MeterCode;
                        row["Serial"] = loadProfile.Serial;
                        row["PlantCode"] = loadProfile.PlantCode;
                        row["DateTime"] = loadProfile.DateTime;
                        row["Quality"] = loadProfile.Quality;
                        row["Stream"] = loadProfile.Stream;
                        row["DataType"] = loadProfile.DataType;
                        row["Energy"] = loadProfile.Energy;
                        row["Units"] = loadProfile.Units;

                        dt.Rows.Add(row);
                    }

                    sqlBulkCopy.DestinationTableName = "Tous";
                    sqlBulkCopy.ColumnMappings.Add("MeterCode", "MeterCode");
                    sqlBulkCopy.ColumnMappings.Add("Serial", "Serial");
                    sqlBulkCopy.ColumnMappings.Add("PlantCode", "PlantCode");
                    sqlBulkCopy.ColumnMappings.Add("DateTime", "DateTime");
                    sqlBulkCopy.ColumnMappings.Add("Quality", "Quality");
                    sqlBulkCopy.ColumnMappings.Add("Stream", "Stream");
                    sqlBulkCopy.ColumnMappings.Add("DataType", "DataType");
                    sqlBulkCopy.ColumnMappings.Add("Energy", "Energy");
                    sqlBulkCopy.ColumnMappings.Add("Units", "Units");

                    await sqlBulkCopy.WriteToServerAsync(dt);
                    var meterCodes = loadProfiles.Select(x => x.MeterCode).Distinct();
                    await _computedAggregationRepository.UpdateAggregation(meterCodes);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return 0;
            }

            return 1;
        }
    }
}