﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERM.Entities.Repositories
{
    public interface IBulkImportRepository<T> where T: class
    {
        Task<int> Import(IEnumerable<T> loadProfiles);
    }
}