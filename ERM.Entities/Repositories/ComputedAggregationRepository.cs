﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ERM.Entities.Models;

namespace ERM.Entities.Repositories
{
    public class ComputedAggregationRepository
    {
        private readonly ErmDbContext _dbContext;

        public ComputedAggregationRepository(ErmDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> UpdateAggregation(IEnumerable<int> meterCodes)
        {
            foreach (var meterCode in meterCodes)
            {
                var result = await _dbContext.LoadProfiles.Where(x => x.MeterPointCode == meterCode).ToListAsync();
                if (!result.Any())
                {
                    continue;
                }

                var groupedResult = result.GroupBy(x => new { x.MeterPointCode, x.DataType, x.DateTime.Date }).Select(x => new ComputedAggregation() {
                    MeterCode = x.Key.MeterPointCode,
                    Date = x.Key.Date,
                    DataType = x.Key.DataType,
                    Min = x.Min(aa => aa.DataValue),
                    Max = x.Max(aa => aa.DataValue),
                    Avg = x.Average(aa => aa.DataValue)
                }).ToList();

                foreach (var item in groupedResult)
                {
                    var caResult = _dbContext.ComputedAggregations
                        .Where(x => x.MeterCode == item.MeterCode && x.DataType == item.DataType && x.Date == item.Date);
                    if (!await caResult.AnyAsync())
                    {
                        _dbContext.ComputedAggregations.Add(item);
                    }
                    else
                    {
                        var ca = await caResult.SingleAsync();
                        ca.Min = item.Min;
                        ca.Max = item.Max;
                        ca.Avg = item.Avg;
                    }
                }
            }

            await _dbContext.SaveChangesAsync();

            return 1;
        }
    }
}
