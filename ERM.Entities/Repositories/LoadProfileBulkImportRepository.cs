﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ERM.Entities.Models;

namespace ERM.Entities.Repositories
{
    public class LoadProfileBulkImportRepository : IBulkImportRepository<LoadProfile>
    {
        private readonly ErmDbContext _dbContext;
        private readonly ComputedAggregationRepository _computedAggregationRepository;

        public LoadProfileBulkImportRepository(ErmDbContext dbContext, ComputedAggregationRepository computedAggregationRepository)
        {
            _dbContext = dbContext;
            _computedAggregationRepository = computedAggregationRepository;
        }

        public async Task<int> Import(IEnumerable<LoadProfile> loadProfiles)
        {
            try
            {
                using (var sqlBulkCopy = new SqlBulkCopy(_dbContext.Database.Connection.ConnectionString,
                    copyOptions: SqlBulkCopyOptions.UseInternalTransaction))
                {
                    var dt = new DataTable();
                    dt.Columns.Add("MeterPointCode", typeof(int));
                    dt.Columns.Add("SerialNumber", typeof(int));
                    dt.Columns.Add("PlantCode", typeof(string));
                    dt.Columns.Add("DateTime", typeof(DateTime));
                    dt.Columns.Add("DataType", typeof(string));
                    dt.Columns.Add("DataValue", typeof(decimal));
                    dt.Columns.Add("Units", typeof(string));
                    dt.Columns.Add("Status", typeof(string));

                    foreach (var loadProfile in loadProfiles)
                    {
                        var row = dt.NewRow();
                        row["MeterPointCode"] = loadProfile.MeterPointCode;
                        row["SerialNumber"] = loadProfile.SerialNumber;
                        row["PlantCode"] = loadProfile.PlantCode;
                        row["DateTime"] = loadProfile.DateTime;
                        row["DataType"] = loadProfile.DataType;
                        row["DataValue"] = loadProfile.DataValue;
                        row["Units"] = loadProfile.Units;
                        row["Status"] = loadProfile.Status;

                        dt.Rows.Add(row);
                    }

                    sqlBulkCopy.DestinationTableName = "LoadProfiles";
                    sqlBulkCopy.ColumnMappings.Add("MeterPointCode", "MeterPointCode");
                    sqlBulkCopy.ColumnMappings.Add("SerialNumber", "SerialNumber");
                    sqlBulkCopy.ColumnMappings.Add("PlantCode", "PlantCode");
                    sqlBulkCopy.ColumnMappings.Add("DateTime", "DateTime");
                    sqlBulkCopy.ColumnMappings.Add("DataType", "DataType");
                    sqlBulkCopy.ColumnMappings.Add("DataValue", "DataValue");
                    sqlBulkCopy.ColumnMappings.Add("Units", "Units");
                    sqlBulkCopy.ColumnMappings.Add("Status", "Status");

                    await sqlBulkCopy.WriteToServerAsync(dt);
                    var meterCodes = loadProfiles.Select(x => x.MeterPointCode).Distinct();
                    await _computedAggregationRepository.UpdateAggregation(meterCodes);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return 0;
            }

            return 1;
        }

        
    }
}
