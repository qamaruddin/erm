﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using ERM.Entities;
using ERM.Entities.Models;
using ERM.Entities.Repositories;

namespace ERM.Client.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<ErmDbContext>().AsSelf()
                .As<DbContext>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof(EfRepository<>))
                .As(typeof(IAsyncRepository<>))
                .InstancePerRequest();
            builder.RegisterType(typeof(LoadProfileBulkImportRepository))
               .As(typeof(IBulkImportRepository<LoadProfile>))
               .InstancePerRequest();
            builder.RegisterType(typeof(TouBulkImportRepository))
               .As(typeof(IBulkImportRepository<Tou>))
               .InstancePerRequest();
            builder.RegisterType(typeof(ComputedAggregationRepository))
               .InstancePerRequest();
            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
