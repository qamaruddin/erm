﻿using System;
using System.Data.Entity;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using ERM.Entities.Models;
using ERM.Entities.Repositories;
using ERM.Services;

namespace ERM.Client.Api.Controllers
{
    public class LoadProfileController : ApiController
    {
        private readonly IAsyncRepository<ComputedAggregation> _repository;
        private readonly IBulkImportRepository<LoadProfile> _bulkImportRepository;

        public LoadProfileController(IAsyncRepository<ComputedAggregation> repository, IBulkImportRepository<LoadProfile> bulkImportRepository)
        {
            _repository = repository;
            _bulkImportRepository = bulkImportRepository;
        }
        

        public async Task<IHttpActionResult> Get([FromUri]DateTime date, [FromUri]int meter, [FromUri]string datatype)
        {
            try
            {
                var data = await _repository
                                .GetWhere(x => x.MeterCode == meter && DbFunctions.TruncateTime(x.Date) == date && x.DataType == datatype);
                return Ok(data);
            }
            catch (Exception)
            {
                return InternalServerError(new Exception("An error has occured"));
            }
        }

        public async Task<IHttpActionResult> Post()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            try
            {
                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var fileContent in filesReadToProvider.Contents)
                {
                    var contentType = fileContent.Headers.ContentType.MediaType;
                    if (contentType.ToLower()!=@"text/csv")
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    var fileBytes = await fileContent.ReadAsStreamAsync();
                    var result = await new DataImporter<LoadProfile, LoadProfileMap>(_bulkImportRepository).Import(fileBytes);
                }
               
                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError(new Exception("An error has occured"));
            }
        }
    }
}
