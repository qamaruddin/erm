I have made several assumptions while building this project.

1. The data import is append only not update of existing set of data.
2. There will not be a limitation in terms of import file size.
3. The csv file structure and layout will remain consistent.

The solution has few projects.
1.ERM.Client.Api - This is the API project which has two sets of endpoints seperated by type of csv import ( LU, TOU )
		   The endpoints basically has import and get data operations. Please see attached postman files.
		   The api is published at https://ermdata.azurewebsites.net
2.ERM.Client.Local - This is just a console application which allows us to upload csv files based on set root directory to search for csv files.
		     We further divide the files based on their nature (LP, TOU)
3.ERM.Entities - This is basically persistent project which maintains entity object, migrations and repositories.
4.ERM.Services - This is a mediator service between controller and enitities. It mainly contain logics about parsing the csv files.

