﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using ERM.Entities;
using ERM.Entities.Models;
using ERM.Entities.Repositories;
using ERM.Services;

namespace ERM.Client.Local
{
    class Program
    {
        static void Main(string[] args)
        {
            var dbContext = new ErmDbContext();
            var computedAggregationRepository = new ComputedAggregationRepository(new ErmDbContext());
            var lpImporter = new DataImporter<LoadProfile, LoadProfileMap>(new LoadProfileBulkImportRepository(dbContext, computedAggregationRepository));
            var touImporter = new DataImporter<Tou, TouMap>(new TouBulkImportRepository(dbContext, computedAggregationRepository));

            var allFiles = Directory.GetFiles(ConfigurationManager.AppSettings["UploadPath"], "*.csv");
            var lpFiles = allFiles.Where(x => Path.GetFileName(x).StartsWith("LP_"));
            var touFiles = allFiles.Where(x => Path.GetFileName(x).StartsWith("TOU_"));

            foreach (var lpFile in lpFiles)
            {
                var memoryStream = new MemoryStream(File.ReadAllBytes(lpFile));
                var response = lpImporter.Import(memoryStream).GetAwaiter().GetResult().Status;
                Console.WriteLine($"{Path.GetFileName(lpFile)} {response.ToString()}");
            }

            foreach (var touFile in touFiles)
            {
                var memoryStream = new MemoryStream(File.ReadAllBytes(touFile));
                var response = touImporter.Import(memoryStream).GetAwaiter().GetResult().Status;
                Console.WriteLine($"{Path.GetFileName(touFile)} {response.ToString()}");
            }

            Console.ReadLine();
        }
    }
}
